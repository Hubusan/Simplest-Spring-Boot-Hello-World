FROM openjdk:11-jre-slim-buster
ADD target/example.smallest-0.0.1-SNAPSHOT.war app.war
EXPOSE 11130
ENTRYPOINT exec java $JAVA_OPTIONS -Djava.security.egd=file:/dev/./urandom -Dserver.port=11130 -jar app.war
